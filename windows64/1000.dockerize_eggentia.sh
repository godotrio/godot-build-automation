#!/bin/bash

# You are almighty root in /tmp
# Godot's source is in /var/godot

# -----

set -eu

#ls -la /var
#ls -la /var/godot

echo "Building Essentia on Windows64"

apt-get --quiet=2 --assume-yes install \
    build-essential \
    git \
    curl \
    wget \
    cmake \
    yasm \
    libboost-all-dev \
    gcc-mingw-w64-x86-64 \
    g++-mingw-w64-x86-64
    

git clone https://github.com/Goutte/essentia.git /var/essentia
cd /var/essentia
git checkout eggentia
cd /

echo "Building Essentia's dependencies…"

BITS=64 \
/var/essentia/packaging/build_3rdparty_static_win32.sh


#exit 0


echo "Cloning the Eggentia module…"

git clone https://framagit.org/godotrio/godot-module-eggentia.git /var/godot-module-eggentia


echo "Installing the Eggentia module…"

ln -s /var/godot-module-eggentia/eggentia /var/godot/modules/eggentia



echo "Building Essentia itself…"

# --with-static-examples
#   LOOKS SUPERFLUOUS? IT ISN'T. IT'S REQUIRED.  Ask the _imp_ why.

cd /var/essentia
./waf configure \
    --verbose \
    --build-static \
    --static-dependencies \
    --with-static-examples \
    --cross-compile-mingw32 \
    --cross-compile-mingw32-prefix=x86_64-w64-mingw32
./waf --verbose
# Perhaps we don't need to install here
./waf install --verbose
cd /


