#!/bin/bash

echo "Building godot…"

cd /var/godot

# CXX Fix
# -------
# Can|should probably be patched upstream.

# module_etc_enabled=no
# ---------------------
# `std::future` breaks.
# Tried libboost, but how to pass it here ? 
# CCFLAGS: Custom flags for both the C and C++ compilers
# CXXFLAGS: Custom flags for the C++ compiler
# CFLAGS: Custom flags for the C compiler
# LINKFLAGS: Custom flags for the linker

scons \
    CXX=x86_64-w64-mingw32-g++ \
    dev=yes \
    platform=windows \
    bits=64 \
    tools=yes \
    use_static_cpp=yes \
    no_editor_splash=yes \
    module_etc_enabled=no \
    debug_symbols=no

scons \
    CXX=x86_64-w64-mingw32-g++ \
    dev=yes \
    platform=windows \
    bits=64 \
    tools=no \
    use_static_cpp=yes \
    module_etc_enabled=no \
    target=release

scons \
    CXX=x86_64-w64-mingw32-g++ \
    dev=yes \
    platform=windows \
    bits=64 \
    tools=no \
    use_static_cpp=yes \
    module_etc_enabled=no \
    target=release_debug




echo "Finished building!"
