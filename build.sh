#!/bin/bash

# TODO getopts plz

SERVICE_PREFIX="godot_bakery_"
ALL_TARGETS=("linux64" "linux32" "windows64" "windows32")

TARGETS=("linux64")
TARGETS=("windows64") # fixme
GODOT_VERSION="3.1.1-stable"

# ODOT


VERBOSE=false
HELP=false
PLATFORM=all
USE_CACHE=false

function usage {
	cat <<EOUSAGE
Build godot with extra modules.
Expect a stdout log of ~30k lines.

Usage: $(basename "$0") [OPTION]...

  -h          
  --help      Print this help.

  -p=all
  --platform=all
              The platform to build for (or against?).
              So far we provide:
              - all
              - linux64
              - windows64
  
  -c
  --cache     Don't rebuild the docker images.
              Use the cached images, it's faster.
              Careful, your scripts are copied during bootstrap
              and won't run the latest version.

This is experimental.  Contributions and advices welcome.
Perhaps the whole stack (bash+docker) is wrong, is somewhat incestuous.
EOUSAGE

	exit 2
}

# -----

MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
ME="$(basename -- $0)"

# -----

# from https://gist.github.com/cosimo/3760587

OPTS=`getopt -o vhcp: --long verbose,help,cache,platform: -n 'parse-options' -- "$@"`

if [ $? != 0 ] ; then echo "Failed parsing options." >&2 ; exit 1 ; fi

echo "$OPTS"
eval set -- "$OPTS"

while true; do
  case "$1" in
    -v | --verbose )    VERBOSE=true;    shift ;;
    -h | --help )       HELP=true;       shift ;;
    -c | --cache )      USE_CACHE=true;  shift ;;
    -p | --platform )   PLATFORM="$2";   shift;  shift ;;
    -- ) shift; break ;;
    * )  break ;;
  esac
done


# -----

if [ true = $HELP ] ; then
    usage
    exit 42
fi


if [ true = $USE_CACHE ] ; then
    echo "Non-implemented feature --cache.  Do code it."
    exit 42
fi


if [ ! "${PLATFORM}" == "all" ] ; then
    echo "Targeting platform ${PLATFORM} only."
    TARGETS=(${PLATFORM})
fi


# -----

cd ${MY_DIR}

# Grab Godot only if it's not there already.
# We can edit our local godot copy ad nauseam (useful for debugging),
# switch branches, etc. 
if [ ! -d godot ] ; then
    echo "First time?  Let's start by cloning Godot in ./godot…"
    git clone https://github.com/godotengine/godot.git godot
    cd godot
    git checkout ${GODOT_VERSION}
    cd ..
    cat README.md
    echo "------------------------------------------------"
    echo "All done!  You can also mod godot's source for a quick fix."
    echo "Godot is in ./godot"
    echo "You can for example switch to another branch."
    echo "You'll have to rebuild everything if you change something."
    echo "Best not add your modules in there, though."
    echo "Use configure, dockerize and build scripts instead."
    echo "Just read the Modding section of the README printed above,"
    echo "while the compilation continues."
    echo "… You're here for a while anyways."
    sleep 13
fi


# Copy some configuration around
for TARGET in ${TARGETS[*]} ; do
    cp .env ${TARGET}/
    cp common/* ${TARGET}/
    
    # Urgh. >w<
    cp --recursive --force godot ${TARGET}/
done

# Add one, uncomment
# Let modules do their things
# for SCRIPT in *configure*.sh ; do
#     echo "Running configuration script ${SCRIPT}…"
#     cd ${MY_DIR}
#     bash ${SCRIPT} $@
# done


cd ${MY_DIR}
# docker-compose rm
for TARGET in ${TARGETS[*]} ; do
    echo "Building docker image for ${TARGET}…"
    docker-compose build --no-cache ${SERVICE_PREFIX}${TARGET}
    BUILD_OK=$?
    
    if [ $BUILD_OK -eq 0 ] ; then
        echo "Ok, continuing…  You still there?"
    else 
        echo "BUILD FAILED FOR TARGET '${TARGET}'! (${BUILD_OK})"
        exit $BUILD_OK
    fi
done


cd ${MY_DIR}
for TARGET in ${TARGETS[*]} ; do
    echo "Running docker container for ${TARGET}…"
    docker-compose up ${SERVICE_PREFIX}${TARGET}
done


echo "All done. Look for your builds in ./bin/"
echo "> ls bin"
ls -lah bin
