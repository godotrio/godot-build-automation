#!/bin/bash

echo "Building godot…"

cd /var/godot

scons \
    dev=yes \
    platform=x11 \
    bits=64 \
    use_static_cpp=yes \
    tools=yes \
    no_editor_splash=yes \
    debug_symbols=no

scons \
    dev=yes \
    platform=x11 \
    bits=64 \
    use_static_cpp=yes \
    tools=no \
    target=release

scons \
    dev=yes \
    platform=x11 \
    bits=64 \
    use_static_cpp=yes \
    tools=no \
    target=release_debug


#     use_lto=true
# ↑ yields a bunch of
# godot_bakery_linux64    | /tmp/ccEazPHA.ltrans27.ltrans.o:(.data+0x1e900): undefined reference to `.L48387'
# and a failure. can't find missing package for now
# any clues, anyone?


echo "Finished building!"
