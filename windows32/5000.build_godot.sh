#!/bin/bash

echo "Building godot for windows32…"

cd /var/godot

# CXX=…
# -----
# Fix this upstream.

# module_etc_enabled=no
# ---------------------
# `std::future` breaks.
# Tried libboost, but how to pass it here ? 
# CCFLAGS: Custom flags for both the C and C++ compilers
# CXXFLAGS: Custom flags for the C++ compiler
# CFLAGS: Custom flags for the C compiler
# LINKFLAGS: Custom flags for the linker

scons \
    CXX=i686-w64-mingw32-g++ \
    dev=yes \
    platform=windows \
    bits=32 \
    tools=yes \
    use_static_cpp=yes \
    no_editor_splash=yes \
    module_etc_enabled=no \
    debug_symbols=no

scons \
    CXX=i686-w64-mingw32-g++ \
    dev=yes \
    platform=windows \
    bits=32 \
    tools=no \
    use_static_cpp=yes \
    module_etc_enabled=no \
    target=release

scons \
    CXX=i686-w64-mingw32-g++ \
    dev=yes \
    platform=windows \
    bits=32 \
    tools=no \
    use_static_cpp=yes \
    module_etc_enabled=no \
    target=release_debug



echo "Finished building godot for windows32!"
