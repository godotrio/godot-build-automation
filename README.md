
## Experimental Godot building automation


- [x] linux64 (ok)
- [X] windows64 (no etc2, no chromaprint)
- [X] windows32 (idem)
- [ ] linux32
- [ ] android?

> Any of these tasks is up for grabs.  No promises.
> ETC2 issue can probably be fixed (libboost?)

---

[Godot](https://godotengine.org) is excellent.

Here you'll find some naive attempts at compiling godot from source, with extra modules.  The point is to clone this and add custom modules easily.


You'll need bash, docker and docker-compose.

    apt install bash docker.io docker-compose

Then run
    
    ./build.sh | tee build.log

Your binaries will be baked in `./bin`.  It takes a while.
[Read](https://qntm.org/responsibility) a [book](https://hpmor.com).
Best use `tee`, the log gets pretty long and errors may hide.


You can also target a specific platform:

    ./build.sh --platform linux64


---

## Modding

Vanilla Godot is great.
But Godot with extra modules becomes super-duper.

This project allows you to automatize building the modules dependencies.

So far, there are three hooks:

- **I. Configure**
  Run on the host, as yourself, in this project's directory, right before running docker. I have not used that yet. It's commented out.
  
- **II. Dockerize**
  Run as root in the `ubuntu:bionic` container, in `/tmp`.
  Godot is already cloned in `/var/godot`.
  A good place to add system dependencies via `apt install`.
  
- **III. Build**
  Run during the CMD of the docker image.
  Godot is built during this phase, and you can hook either
  before or after, since the order of the scripts is the order of `ls`, and godot's script's name is prefixed with `5000`.

Creating a hook is creating a bash `.sh` file with either `configure`, `dockerize` or `build` in the name, and a numerical prefix on _four_ digits with a _dot_ like `0042.`.

> This also means that **these words are reserved**. 
> And so is the `_all` suffix, for good measure.

Example of correct filename: `linux64/1000.dockerize_eggentia.sh`.


Place your bash script in the appropriate target directory (eg: `windows64/`),
or in `common/` where it will be shared with all.



